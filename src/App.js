import React from "react";
import { Route, Routes } from "react-router-dom";
import Create from "./components/Create";
import Header from "./components/Header";
import Home from "./components/Home";

const App = () => {
  return (
    <>
      <Header />
      <Routes>
        <Route path="/" element={<Home />} />
        <Route path="/create" element={<Create />} />
      </Routes>
    </>
  );
};

export default App;
