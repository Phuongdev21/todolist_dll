import React, { useState, useEffect } from "react";
import request from "../services/request";

const Home = () => {
  const [todos, settodos] = useState([]);
  const [input, setInput] = useState("");
  const [reRender, setReRender] = useState(false);
  const [status, setStatus] = useState("add");
  const [id, setId] = useState("");

  useEffect(() => {
    const fetchTodos = async () => {
      const todosRes = await request.get("/todos");
      if (todosRes) settodos(todosRes);
    };
    fetchTodos();
  }, [reRender]);

  const handleAdd = async () => {
    if (status === "add") {
      const todoRes = await request.post("/todo/create", { task: input });
      if (todoRes) {
        setInput("");
        setReRender(!reRender);
        return alert("Thêm thành công");
      }
    } else {
      const todoRes = await request.post(
        `/todo/${id}/update`,
        { task: input },
        "PUT"
      );
      if (todoRes) {
        setInput("");
        setStatus("add");
        setReRender(!reRender);
        return alert("Update thành công");
      }
    }
  };

  const handleEdit = async (todo) => {
    setInput(todo.task);
    setStatus("update");
    setId(todo._id);
  };

  const handleDelete = async (id) => {
    const todoRes = await request.post(
      `/todo/${id}/delete`,
      { task: input },
      "DELETE"
    );
    if (todoRes) {
      setReRender(!reRender);
      return alert("Xóa thành công");
    }
  };

  return (
    <div>
      <h1>List todo</h1>
      <input
        type="text"
        value={input}
        onChange={(e) => setInput(e.target.value)}
      />
      <button onClick={handleAdd}>{status}</button>
      <ul>
        {todos?.map((todo, index) => (
          <li key={index} style={{ marginBottom: "10px" }}>
            <span style={{ marginRight: "20px" }}>{todo.task}</span>
            <button onClick={() => handleEdit(todo)}>Edit</button>
            <button onClick={() => handleDelete(todo._id)}>Delete</button>
          </li>
        ))}
      </ul>
    </div>
  );
};

export default Home;
