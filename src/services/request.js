const request = {};
const HOST = "http://localhost:8000/api";

request.post = async (url, body, method="POST") => {
  const response = await fetch(HOST + url, {
    method: method,
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify(body),
  });
  const result = await response.json();
  return result;
};

request.get = async (url) => {
  const todosRes = await fetch(HOST + url);
  const data = await todosRes.json();
  return data;
};

export default request;
